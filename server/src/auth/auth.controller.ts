import { Controller, Req, UseGuards } from '@nestjs/common';
import { Body, Post, Res, HttpStatus } from '@nestjs/common';
import { Response } from 'express';

import { AuthService } from './auth.service';
import { LoginAdminDTO } from './dto/login.dto';
import { AuthGuard } from './auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly service: AuthService) {}

  @Post('login')
  async login(@Body() loginAdminDto: LoginAdminDTO, @Res() res: Response) {
    try {
      const { email, password } = loginAdminDto;
      if (!email || !password) {
        return res.status(HttpStatus.BAD_REQUEST).json({
          success: false,
          result: null,
          message: 'not all fields have been entered.',
        });
      }
      const { token, admin } = await this.service.signIn(email, password);

      return res.status(HttpStatus.OK).json({
        success: true,
        result: {
          token,
          admin: {
            id: admin._id,
            name: admin.name,
            isLoggedIn: admin.isLoggedIn,
          },
        },
        message: 'Successfully login admin',
      });
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        success: false,
        result: null,
        message: error.message,
        error: error,
      });
    }
  }

  @UseGuards(AuthGuard)
  @Post('logout')
  async logout(@Req() req: any, @Res() res: Response) {
    try {
      await this.service.signOut(req.admin._id);
      res.clearCookie('token');
      res.json({ isLoggedOut: true });
    } catch (error) {
      res.json({ isLoggedOut: false });
    }
  }
}
