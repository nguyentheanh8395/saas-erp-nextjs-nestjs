/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AdminService } from 'src/admin/admin.service';
import { compareHash } from 'src/utils';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private adminService: AdminService,
    private jwtService: JwtService,
  ) {}

  async signIn(email: string, pass: string) {
    const existingAdmin = await this.adminService.findByEmail(email);
    const isMatch = existingAdmin
      ? await compareHash(pass, existingAdmin?.password)
      : false;

    if (!existingAdmin || !isMatch) {
      throw new UnauthorizedException();
    }
    const id = existingAdmin._id.valueOf();
    const payload = { id };
    const token = await this.jwtService.signAsync(payload);
    const admin = await this.adminService.setIsLoggedIn(id);
    return { token, admin };
  }

  async signOut(id: string) {
    return await this.adminService.setIsLoggedIn(id, false);
  }
}
