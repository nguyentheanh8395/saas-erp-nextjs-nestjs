import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';

@Schema()
export class Role {
  @Prop({ default: false })
  removed: boolean;
  @Prop({ lowercase: true, trim: true, required: true })
  codeName: string;
  @Prop({ trim: true, required: true })
  displayName: string;
  @Prop({ trim: true })
  dashboardType: string;
  @Prop({ lowercase: true, trim: true })
  authorizedPages: Array<string>;
  @Prop({ default: Date.now() })
  createdAt: Date;
  @Prop({ type: SchemaTypes.ObjectId })
  permissions: Types.ObjectId;
}

export type RoleDocument = Role & Document;

export const RoleSchema = SchemaFactory.createForClass(Role);
