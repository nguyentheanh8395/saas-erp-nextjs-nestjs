import { Module } from '@nestjs/common';
import { PaymentModeController } from './payment-mode.controller';
import { PaymentModeService } from './payment-mode.service';

@Module({
  controllers: [PaymentModeController],
  providers: [PaymentModeService]
})
export class PaymentModeModule {}
