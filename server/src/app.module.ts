import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { AdminModule } from './admin/admin.module';
import { AuthModule } from './auth/auth.module';
import { RoleModule } from './role/role.module';
import { EmployeeModule } from './employee/employee.module';
import { PaymentModeModule } from './payment-mode/payment-mode.module';
import { ClientModule } from './client/client.module';
import { InvoiceModule } from './invoice/invoice.module';
import { ItemModule } from './item/item.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/nest'),
    AdminModule,
    AuthModule,
    RoleModule,
    EmployeeModule,
    PaymentModeModule,
    ClientModule,
    InvoiceModule,
    ItemModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
