import * as bcrypt from 'bcrypt';

const generateHash = (password: string) => {
  return bcrypt.hashSync(password, bcrypt.genSaltSync());
};

const compareHash = async (hash: string, value: string) => {
  return await bcrypt.compare(hash, value);
};
export { generateHash, compareHash };
