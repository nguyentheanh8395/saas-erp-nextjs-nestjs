export class CreateAdminDTO {
  email: string;
  password: string;
  name: string;
  surName: string;
}
