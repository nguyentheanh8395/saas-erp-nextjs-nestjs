export interface IListParam {
  page?: number;
  limit?: number;
}
