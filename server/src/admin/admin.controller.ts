/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Body,
  Post,
  Res,
  HttpStatus,
  Get,
  Param,
  UseGuards,
  Query,
} from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { Response } from 'express';

import { AdminService } from './admin.service';
import { CreateAdminDTO } from './dto/create.dto';
import { AuthGuard } from '../auth/auth.guard';
import { generateHash } from 'src/utils';
import { IListParam } from './dto/param.dto';

@Controller('admin')
export class AdminController {
  constructor(private readonly service: AdminService) {}

  @UseGuards(AuthGuard)
  @Post('create')
  async create(@Body() createAdminDto: CreateAdminDTO, @Res() res: Response) {
    try {
      const { email, password } = createAdminDto;
      if (!email || !password) {
        return res.status(HttpStatus.BAD_REQUEST).json({
          success: false,
          result: null,
          message: "Email or password fields they don't have been entered.",
        });
      }
      const existingAdmin = await this.service.findByEmail(email);
      if (existingAdmin) {
        return res.status(HttpStatus.BAD_REQUEST).json({
          success: false,
          result: null,
          message: 'An account with this email already exists.',
        });
      }

      if (password.length < 8) {
        return res.status(HttpStatus.BAD_REQUEST).json({
          success: false,
          result: null,
          message: 'The password needs to be at least 8 characters long.',
        });
      }
      const passwordHash = generateHash(password);
      const createAdminPayload = { ...createAdminDto, password: passwordHash };
      const rs = await this.service.create(createAdminPayload);
      if (!rs) {
        return res.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).json({
          success: false,
          result: null,
          message: "Document couldn't save correctly",
        });
      }
      return res.status(HttpStatus.CREATED).json({
        success: true,
        result: {
          _id: rs._id,
          enabled: rs.enabled,
          email: rs.email,
          name: rs.name,
          surname: rs.surName,
          photo: rs.photo,
          role: rs.role,
        },
        message: 'Admin document save correctly',
      });
    } catch (error) {
      console.log({ error });
      return error;
    }
  }

  @UseGuards(AuthGuard)
  @Get('read/:id')
  async read(@Param('id') id, @Res() res: Response) {
    try {
      const admin = await this.service.findActiveAccountById(id);
      if (!admin) {
        return res.status(HttpStatus.NOT_FOUND).json({
          success: false,
          result: null,
          message: 'No document found by this id: ' + id,
        });
      }
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...restAdmin } = admin;
      return res.status(HttpStatus.OK).json({ admin: restAdmin });
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        success: false,
        result: null,
        message: 'Oops there is an Error',
      });
    }
  }

  @UseGuards(AuthGuard)
  @Get('list')
  async list(@Query() query: IListParam, @Res() res: Response) {
    try {
      const { page = 1, limit = 10 } = query;
      const skip = (page - 1) * limit;
      const [results, count] = await Promise.all([
        this.service.getListAdmin(limit, skip),
        this.service.countAdmins(),
      ]);
      const result = results.map((admin) => {
        const { password, permissions, ...rest } = admin.toObject();
        return rest;
      });
      const pages = Math.ceil(count / limit);
      const pagination = { page, pages, count };

      return res
        .status(
          !result.length
            ? HttpStatus.NON_AUTHORITATIVE_INFORMATION
            : HttpStatus.OK,
        )
        .json({
          success: true,
          result,
          pagination,
          message: !result.length
            ? 'Collection is Empty'
            : 'Successfully found all documents',
        });
    } catch (error) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        success: false,
        result: [],
        message: 'Opps there is an Error',
      });
    }
  }

  @UseGuards(AuthGuard)
  @Get('profile')
  async profile(@Res() res: Response) {
    return res.status(HttpStatus.OK).json({
      success: false,
      result: [],
      message: 'profile',
    });
  }
}
