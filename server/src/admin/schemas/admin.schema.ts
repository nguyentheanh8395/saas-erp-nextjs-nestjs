import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';

@Schema()
export class Admin {
  @Prop({ default: false })
  removed: boolean;
  @Prop({ default: true })
  enabled: boolean;
  @Prop({ required: true })
  password: string;
  @Prop({ unique: true, lowercase: true, trim: true, required: true })
  email: string;
  @Prop({ required: true, lowercase: true })
  name: string;
  @Prop({ required: true, lowercase: true })
  surName: string;
  @Prop({ trim: true })
  photo: string;
  @Prop({ default: Date.now() })
  createdAt: Date;
  @Prop({ default: false })
  hasCustomPermissions: boolean;
  @Prop()
  isLoggedIn: boolean;
  @Prop({ type: SchemaTypes.ObjectId })
  role: Types.ObjectId;
  @Prop({ type: SchemaTypes.ObjectId })
  permissions: Types.ObjectId;
}

export type AdminDocument = Admin & Document;

export const AdminSchema = SchemaFactory.createForClass(Admin);
