import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Admin, AdminDocument } from './schemas/admin.schema';
import { CreateAdminDTO } from './dto/create.dto';

@Injectable()
export class AdminService {
  constructor(
    @InjectModel(Admin.name) private readonly model: Model<AdminDocument>,
  ) {}
  async create(createAdminDTO: CreateAdminDTO): Promise<AdminDocument> {
    return await new this.model({
      ...createAdminDTO,
      createdAt: new Date(),
    }).save();
  }
  async findByEmail(email: string): Promise<AdminDocument> {
    return await this.model.findOne({ email });
  }
  async findActiveAccountById(id: string): Promise<AdminDocument> {
    try {
      const o_id = new Types.ObjectId(id);
      return (
        await this.model.findOne({ _id: o_id, removed: false })
      ).toObject();
    } catch (error) {
      return undefined;
    }
  }
  async setIsLoggedIn(
    id: string,
    isLoggedIn?: boolean,
  ): Promise<AdminDocument> {
    return await this.model.findByIdAndUpdate(
      id,
      { isLoggedIn: isLoggedIn ?? true },
      { new: true },
    );
  }
  async countAdmins(): Promise<number> {
    return await this.model.find({ removed: false }).count();
  }
  async getListAdmin(
    limit?: number,
    skip?: number,
  ): Promise<Array<AdminDocument>> {
    return await this.model
      .find({ removed: false })
      .skip(skip)
      .limit(limit)
      .sort({ created: 'desc' })
      // .select(['-password'])
      .populate(['permissions', 'role']);
  }
}
