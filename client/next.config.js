/** @type {import('next').NextConfig} */
const nextConfig = {
  serverRuntimeConfig: {
    nextAuthURL: process.env.NEXTAUTH_URL,
    nextAuthSecret: process.env.NEXTAUTH_SECRET,
  },
  publicRuntimeConfig: {
    baseApiURL: process.env.BASE_API_URL,
  },
  env: {
    baseApiURL: process.env.BASE_API_URL,
  },
};

module.exports = nextConfig;
