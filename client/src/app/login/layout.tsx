import { PropsWithChildren } from "react";
import { Layout, Row, Col } from "antd";
import { SideContentOrganism } from "@/components";

const AuthLayout = ({ children }: PropsWithChildren) => {
  return (
    <Layout>
      <Row>
        <Col
          className="!min-h-screen"
          xs={{ span: 0, order: 2 }}
          sm={{ span: 0, order: 2 }}
          md={{ span: 11, order: 1 }}
          lg={{ span: 12, order: 1 }}
        >
          <SideContentOrganism />
        </Col>
        <Col
          className="!min-h-screen bg-white"
          xs={{ span: 24, order: 1 }}
          sm={{ span: 24, order: 1 }}
          md={{ span: 13, order: 2 }}
          lg={{ span: 12, order: 2 }}
        >
          {children}
        </Col>
      </Row>
    </Layout>
  );
};

export default AuthLayout;
