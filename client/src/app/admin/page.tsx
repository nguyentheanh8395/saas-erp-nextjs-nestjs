import { AdminCrudTemplate } from "@/components";

const AdminPage = () => {
  const dataTableTitle = "Admin Lists";
  const entity = "admin";
  const ADD_NEW_ENTITY = "Add new admin";

  const dataTableColumns = [
    { title: "Name", dataIndex: "name" },
    { title: "Surname", dataIndex: "surname" },
    { title: "Email", dataIndex: "email" },
    { title: "Role", dataIndex: ["role", "displayName"] },
  ];
  const config = {
    entity,
    dataTableTitle,
    dataTableColumns,
    ADD_NEW_ENTITY,
  };
  return <AdminCrudTemplate config={config} />;
};

export default AdminPage;
