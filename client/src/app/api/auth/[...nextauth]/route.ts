import axios from "axios";
import type { NextApiRequest, NextApiResponse } from "next";
import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import getConfig from "next/config";

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

const { nextAuthSecret: secret } = serverRuntimeConfig;
const { baseApiURL } = publicRuntimeConfig;

const handler = async function auth(req: NextApiRequest, res: NextApiResponse) {
  // const cookies = {};
  const providers = [
    CredentialsProvider({
      name: "credentials",
      id: "credentials",
      credentials: {
        email: {},
        password: {},
      },
      async authorize(credentials) {
        try {
          if (!credentials?.email || !credentials?.password) {
            throw new Error();
          }
          const { email, password } = credentials || {};

          console.log(baseApiURL);
          const user = await axios.post(`${baseApiURL}/auth/login`, {
            email,
            password,
          });
          if (!user.data.success) {
            return null;
          }
          return { id: "jwt", accessToken: user.data.result.token };
        } catch (error: any) {
          return Promise.reject(error.response?.data ?? error);
        }
      },
    }),
  ];
  const callbacks = {
    jwt: async ({ token, user }: any) => {
      if (user) {
        token.accessToken = user.accessToken;
        // token.accessTokenExpiry = user.data.accessTokenExpiry;
        // token.refreshToken = user.data.refreshToken;
      }

      // const shouldRefreshTime = Math.round(
      //   token.accessTokenExpiry - 60 * 60 * 1000 - Date.now()
      // );

      // If the token is still valid, just return it.
      // if (shouldRefreshTime > 0) {
      //   return Promise.resolve(token);
      // }

      // token = refreshAccessToken(token);
      return Promise.resolve(token);
    },
  };
  return await NextAuth(req, res, {
    providers,
    callbacks,
    session: { strategy: "jwt" },
    // cookies: cookies,
    secret,
  });
};

export { handler as GET, handler as POST };
