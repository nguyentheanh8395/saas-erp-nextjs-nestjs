import { Button, Result } from "antd";
import Link from "next/link";

const NotFoundPage = () => {
  return (
    <div className="flex items-center justify-center min-h-screen">
      <Result
        status={404}
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={
          <Link href={"/"}>
            <Button type="primary">Home</Button>
          </Link>
        }
      />
    </div>
  );
};

export default NotFoundPage;
