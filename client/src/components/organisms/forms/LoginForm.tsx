import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Checkbox, Form, Input } from "antd";
import Link from "next/link";
import { Fragment, useMemo } from "react";

const LoginFormOrganism = () => {
  const emailRules = useMemo(
    () => [{ required: true, message: "Please input your Email!" }],
    []
  );

  const passwordRules = useMemo(
    () => [{ required: true, message: "Please input your Password!" }],
    []
  );
  return (
    <Fragment>
      <Form.Item name="email" rules={emailRules}>
        <Input
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="admin@demo.com"
          autoComplete="email"
          size="large"
        />
      </Form.Item>
      <Form.Item name="password" rules={passwordRules}>
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          placeholder="admin123"
          size="large"
        />
      </Form.Item>
      <div className="flex items-center mb-6 w-full justify-between">
        <Form.Item name="remember" valuePropName="checked" noStyle>
          <Checkbox>Remember me</Checkbox>
        </Form.Item>
        <Link href="/" className="login-form-forgot">
          Forgot password
        </Link>
      </div>
    </Fragment>
  );
};

export default LoginFormOrganism;
