"use client";
import { Divider, Layout, Space, Typography } from "antd";
import Image from "next/image";

const { Content } = Layout;
const { Title, Text } = Typography;

const SideContentOrganism = () => {
  return (
    <Content className="pt-38 px-7.5 pb-7.5 w-full max-w-100 my-0 mx-auto !text-chambray">
      <div className="w-full">
        <picture>
          <Image
            src="/images/logo.png"
            alt="logo"
            width={220}
            height={62}
            className="mx-auto mt-0 mb-10"
          />
        </picture>
        <div className="space40"></div>
        <Title level={3}>Manage your company with :</Title>
        <div className="space20"></div>
        <ul className="pl-0 list-none">
          <li className="list-checked-item">
            <Space direction="vertical">
              <Text strong className="!text-chambray">
                All-in-one tool
              </Text>
              <Text className="!text-chambray">
                Build, run, and scale your apps - end to end
              </Text>
            </Space>
          </li>
          <li className="list-checked-item">
            <Space direction="vertical">
              <Text strong className="!text-chambray">
                Easily add &amp; manage your services
              </Text>
              <Text className="!text-chambray">
                It brings together your tasks, projects, timelines, files and
                more
              </Text>
            </Space>
          </li>
        </ul>
        <Divider />
        <div className="flex justify-between">
          {[
            "/images/logo1.png",
            "/images/logo2.png",
            "/images/logo3.png",
            "/images/logo4.png",
          ].map((item, idx) => (
            <picture key={idx}>
              <Image
                src={item}
                width={48}
                height={48}
                alt={item}
                className="my-0 mx-4 mix-blend-multiply opacity-80 float-left grayscale"
              />
            </picture>
          ))}
        </div>
      </div>
    </Content>
  );
};

export default SideContentOrganism;
