"use client";
import { Layout } from "antd";
import { NavigationMenuMolecule } from "../molecules";
import clsx from "clsx";
import Image from "next/image";
import { useState } from "react";

const Navigation = () => {
  const [showLogoApp, setShowLogoApp] = useState(false);

  return (
    <Layout.Sider
      theme="light"
      collapsible
      className={clsx(
        "border-r !border-pale-blue !border-solid",
        "!z-1000 !bg-white"
      )}
    >
      <div className="h-8 mx-4 mt-4 mb-7.5 pl-1.5 flex">
        <div className="relative w-8">
          <Image
            src="/images/logo-icon.png"
            alt="logo"
            fill
            style={{
              objectFit: "contain",
            }}
          />
        </div>
        <div className="relative pt-1.5 ml-2.5 h-full flex-grow">
          {!showLogoApp && (
            <Image
              src="/images/logo-text.png"
              alt="logo"
              fill
              style={{
                objectFit: "contain",
              }}
            />
          )}
        </div>
      </div>
      <NavigationMenuMolecule />
    </Layout.Sider>
  );
};

export default Navigation;
