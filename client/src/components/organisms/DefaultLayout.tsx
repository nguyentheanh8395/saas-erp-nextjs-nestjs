"use client";
import { Layout } from "antd";
import { PropsWithChildren } from "react";
import { useSession } from "next-auth/react";
import { HeaderContentOrganism, NavigationOrganism } from ".";
import { PageLoaderMolecule } from "..";

const DefaultLayout = ({ children }: PropsWithChildren) => {
  const { status } = useSession();
  if (status === "loading") {
    return <PageLoaderMolecule />;
  }
  if (status !== "unauthenticated") {
    return children;
  }
  return (
    <Layout className="!min-h-screen">
      <NavigationOrganism />
      <Layout className="!min-h-screen">
        <HeaderContentOrganism />
        {children}
      </Layout>
    </Layout>
  );
};

export default DefaultLayout;
