import { Layout } from "antd";
import clsx from "clsx";
import { PropsWithChildren } from "react";

const { Content } = Layout;

const CrudLayout = ({ children }: PropsWithChildren) => {
  return (
    <Layout className="min-h-screen">
      <Content
        className={clsx(
          "mx-auto my-25 px-12 py-10 flex-grow-0 max-w-250",
          "w-full"
        )}
      >
        {children}
      </Content>
    </Layout>
  );
};

export default CrudLayout;
