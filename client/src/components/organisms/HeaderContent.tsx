import { AppstoreOutlined, BellOutlined } from "@ant-design/icons";
import { Avatar, Dropdown } from "antd";

const HeaderContent = () => {
  return (
    <div className="absolute right-0 z-99 pr-7.5">
      <Dropdown
        dropdownRender={() => <div>A</div>}
        trigger={["click"]}
        placement="bottomRight"
      >
        <Avatar src="/images/photo.png" className="avatar" />
      </Dropdown>
      <Avatar icon={<AppstoreOutlined />} className="avatar" />
      <Avatar icon={<BellOutlined />} className="avatar" />
    </div>
  );
};

export default HeaderContent;
