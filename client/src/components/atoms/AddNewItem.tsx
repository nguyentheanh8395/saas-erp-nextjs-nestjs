import { TConfigTable } from "@/utils";
import { Button } from "antd";

type TAddNewItemProps<T> = {
  config: TConfigTable<T>;
};
function AddNewItem<T>({ config }: TAddNewItemProps<T>) {
  const { ADD_NEW_ENTITY } = config;
  return <Button type="primary">{ADD_NEW_ENTITY}</Button>;
}
export default AddNewItem;
