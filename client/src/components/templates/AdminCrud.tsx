"use client";
import { TConfigTable } from "@/utils";
import {
  AddNewItem,
  CrudLayoutOrganism,
  DataTableMolecule,
  DropDownRowMenuAdmin,
} from "..";

type TAdminCrudProps = {
  config: TConfigTable;
};

const AdminCrud = ({ config }: TAdminCrudProps) => {
  return (
    <CrudLayoutOrganism>
      <DataTableMolecule<any>
        config={config}
        DropDownRowMenu={DropDownRowMenuAdmin}
        AddNewItem={AddNewItem}
      />
    </CrudLayoutOrganism>
  );
};

export default AdminCrud;
