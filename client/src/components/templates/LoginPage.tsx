"use client";
import {
  Layout,
  Col,
  Typography,
  Divider,
  Form,
  Button,
  notification,
} from "antd";
import Image from "next/image";
import Link from "next/link";
import { AnimatePresence } from "framer-motion";

import { LoginFormOrganism } from "..";
import { TAdminLoginFormValue } from "@/utils";
import { signIn } from "next-auth/react";
import { useRouter } from "next/navigation";

const { Content } = Layout;
const { Title } = Typography;

const LoginPageTemplate = () => {
  const router = useRouter();
  const handleFinish = (values: TAdminLoginFormValue) => {
    const { email, password } = values;
    signIn("credentials", { email, password, redirect: false }).then(
      (signInResponse) => {
        const { error, ok } = signInResponse ?? {};
        if (ok) {
          router.push("/");
        } else {
          notification.error({ message: "Error", description: error });
        }
      }
    );
  };
  return (
    <AnimatePresence mode="wait" initial={false}>
      <Content className="my-0 mx-auto max-w-27.5 pt-50 px-7.5 pb-7.5">
        <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 0 }} span={0}>
          <picture>
            <Image
              src="/images/logo.png"
              alt="logo"
              width={220}
              height={62}
              className="mx-auto -m-17 mb-10"
            />
          </picture>
          <div className="space50"></div>
        </Col>
        <Title level={1}>Sign in</Title>
        <Divider />
        <div className="site-layout-content">
          <Form
            name="normal_login"
            className="login-form"
            onFinish={handleFinish}
          >
            <LoginFormOrganism />
            <Form.Item>
              <Button
                className="login-form-button"
                size="large"
                type="primary"
                htmlType="submit"
              >
                Log in
              </Button>
              Or <Link href="">register now!</Link>
            </Form.Item>
          </Form>
        </div>
      </Content>
    </AnimatePresence>
  );
};

export default LoginPageTemplate;
