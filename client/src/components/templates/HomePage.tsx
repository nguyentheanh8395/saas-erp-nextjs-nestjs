"use client";
import { Col, Layout, Row } from "antd";
import { useMemo } from "react";
import { TopCardMolecule } from "..";

const { Content } = Layout;

const HomePageTemplate = () => {
  const topCard = useMemo(
    () => [
      {
        title: "Invoice",
        tagColor: "cyan",
        tagContent: "34 000 $",
        prefix: "This month",
      },
      {
        title: "Quote",
        tagColor: "purple",
        tagContent: "34 000 $",
        prefix: "This month",
      },
      {
        title: "Payment",
        tagColor: "green",
        tagContent: "34 000 $",
        prefix: "This month",
      },
      {
        title: "Due Balance",
        tagColor: "red",
        tagContent: "34 000 $",
        prefix: "This month",
      },
    ],
    []
  );
  return (
    <Layout className="site-layout">
      <Content className="px-10 py-7.5 my-17.5 mx-auto w-full max-w-275">
        <Row gutter={[24, 24]}>
          {topCard.map(({ title, tagColor, tagContent, prefix }, idx) => (
            <TopCardMolecule
              key={idx}
              prefix={prefix}
              title={title}
              tagColor={tagColor}
              tagContent={tagContent}
            />
          ))}
        </Row>
        <div className="space30"></div>
        <Row gutter={[24, 24]}>
          <Col className="gutter-row">
            <div className="whiteBox shadow">
              <Row className="p-2.5" gutter={[0, 0]}>
                <div className="p-4">
                  <h3 className="mb-4 text-mainly-blue">Invoice Preview</h3>
                </div>
              </Row>
            </div>
          </Col>
        </Row>
      </Content>
    </Layout>
  );
};

export default HomePageTemplate;
