"use client";
import { TConfigTable, request } from "@/utils";
import { EllipsisOutlined } from "@ant-design/icons";
import { Fragment, ReactElement, useCallback, useEffect, useMemo } from "react";
import { PageHeader } from "@ant-design/pro-layout";
import { Button, Dropdown, Table } from "antd";
import { useRouter } from "next/navigation";
import { v4 as uuidv4 } from "uuid";

type TDataTableProps<T> = {
  config: TConfigTable<T>;
  AddNewItem: React.ComponentType<{ config: TConfigTable<T> }>;
  DropDownRowMenu: React.ComponentType<{ row: T }>;
};

function DataTable<T>({
  config,
  AddNewItem,
  DropDownRowMenu,
}: TDataTableProps<T>) {
  const router = useRouter();
  const { entity, dataTableTitle, dataTableColumns } = config;

  const columns = useMemo(
    () => [
      ...dataTableColumns,
      {
        title: "",
        render: (row: T) => (
          <Dropdown
            dropdownRender={() => <DropDownRowMenu row={row} />}
            trigger={["click"]}
            getPopupContainer={(triggerNode: HTMLElement) => triggerNode}
          >
            <EllipsisOutlined className="cursor-pointer !text-2xl" />
          </Dropdown>
        ),
      },
    ],
    [dataTableColumns]
  );

  const handleDataTableLoad = useCallback(() => {}, []);

  useEffect(() => {
    request.list({ entity });
  }, [entity]);

  const handleBack = useCallback(() => {
    router.back();
  }, [router]);

  return (
    <Fragment>
      <PageHeader
        onBack={handleBack}
        title={dataTableTitle}
        ghost={false}
        className="px-5 py-0"
        extra={[
          <Button key={uuidv4()}>Refresh</Button>,
          <AddNewItem key={uuidv4()} config={config} />,
        ]}
      />
      <Table
        columns={columns}
        dataSource={[{}, {}]}
        onChange={handleDataTableLoad}
      />
    </Fragment>
  );
}

export default DataTable;
