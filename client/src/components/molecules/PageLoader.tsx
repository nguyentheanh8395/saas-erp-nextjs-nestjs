import { Spin } from "antd";
import clsx from "clsx";

const PageLoaderMolecule = () => {
  return (
    <div
      className={clsx(
        "absolute top-0 left-0 w-full h-full bg-slate-500 bg-opacity-20",
        "flex items-center justify-center z-10"
      )}
    >
      <Spin size="large" />
    </div>
  );
};

export default PageLoaderMolecule;
