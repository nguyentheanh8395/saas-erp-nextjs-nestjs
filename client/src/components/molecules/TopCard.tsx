import { Col, Divider, Row, Tag } from "antd";
import clsx from "clsx";

type TTopCardMoleculeProps = {
  prefix: string;
  title: string;
  tagColor: string;
  tagContent: string;
};

const TopCardMolecule = ({
  prefix,
  title,
  tagColor,
  tagContent,
}: TTopCardMoleculeProps) => {
  return (
    <Col
      className="gutter-row"
      xs={{ span: 24 }}
      sm={{ span: 12 }}
      md={{ span: 12 }}
      lg={{ span: 6 }}
    >
      <div
        className={clsx(
          "whiteBox shadow-classic hover:shadow-active",
          "text-gray-35 text-sm h-26.5"
        )}
      >
        <div className="p-4 font-bold text-center flex justify-center">
          <h3 className="text-main text-mainly-blue">{title}</h3>
        </div>
        <Divider className="!p-0 !m-0" />
        <div className="p-4">
          <Row gutter={[0, 0]}>
            <Col className="gutter-row text-left" span={11}>
              <div className="float-left">{prefix}</div>
            </Col>
            <Col className="gutter-row" span={2}>
              <Divider
                type="vertical"
                className="!py-2.5 !px-0 justify-center"
              />
            </Col>
            <Col className="gutter-row flex justify-center" span={11}>
              <Tag color={tagColor} className="my-0 mx-auto justify-center">
                {tagContent}
              </Tag>
            </Col>
          </Row>
        </div>
      </div>
    </Col>
  );
};

export default TopCardMolecule;
