import { Avatar } from "antd";

const ProfileDropdown = () => {
  return (
    <div className="whiteBox shadow min-w-50">
      <div className="p-4 flex">
        <Avatar src="/images/photo.png" className="avatar !float-left" />
      </div>
    </div>
  );
};

export default ProfileDropdown;
