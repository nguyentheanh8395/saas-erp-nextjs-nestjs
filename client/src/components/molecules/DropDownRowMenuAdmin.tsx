import { DeleteOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";
import { Menu } from "antd";
import { v4 as uuidv4 } from "uuid";

function DropDownRowMenuAdmin<T>({}: { row: T }) {
  return (
    <Menu className="w-32">
      <Menu.Item key={uuidv4()} icon={<EyeOutlined />}>
        Show
      </Menu.Item>
      <Menu.Item key={uuidv4()} icon={<EditOutlined />}>
        Edit
      </Menu.Item>
      <Menu.Item key={uuidv4()} icon={<DeleteOutlined />}>
        Delete
      </Menu.Item>
    </Menu>
  );
}

export default DropDownRowMenuAdmin;
