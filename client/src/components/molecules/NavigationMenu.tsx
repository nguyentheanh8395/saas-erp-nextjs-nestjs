import {
  CreditCardOutlined,
  CustomerServiceOutlined,
  DashboardOutlined,
  FileSyncOutlined,
  FileTextOutlined,
  SettingOutlined,
  TeamOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Menu } from "antd";
import Link from "next/link";

const NavigationMenu = () => {
  return (
    <Menu mode="inline" className="!border-none">
      <Menu.Item key={"Dashboard"} icon={<DashboardOutlined />}>
        <Link href={"/"} />
        Dashboard
      </Menu.Item>
      <Menu.Item key={"Customer"} icon={<CustomerServiceOutlined />}>
        <Link href={"/customer"} />
        Customer
      </Menu.Item>
      <Menu.Item key={"Invoice"} icon={<FileTextOutlined />}>
        <Link href={"/invoice"} />
        Invoice
      </Menu.Item>
      <Menu.Item key={"Quote"} icon={<FileSyncOutlined />}>
        <Link href={"/quote"} />
        Quote
      </Menu.Item>
      <Menu.Item key={"PaymentInvoice"} icon={<CreditCardOutlined />}>
        <Link href={"/payment/invoice"} />
        Payment Invoice
      </Menu.Item>
      <Menu.Item key={"Employee"} icon={<UserOutlined />}>
        <Link href={"/employee"} />
        Employee
      </Menu.Item>
      <Menu.Item key={"Admin"} icon={<TeamOutlined />}>
        <Link href={"/admin"} />
        Admin
      </Menu.Item>
      <Menu.SubMenu
        key={"Settings"}
        icon={<SettingOutlined />}
        title="Settings"
      >
        <Menu.Item key={"PaymentMode"}>
          <Link href={"/payment/mode"} />
          Payment Mode
        </Menu.Item>
        <Menu.Item key={"Role"}>
          <Link href={"/role"} />
          Role
        </Menu.Item>
      </Menu.SubMenu>
    </Menu>
  );
};

export default NavigationMenu;
