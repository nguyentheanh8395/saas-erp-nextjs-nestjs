import { EHttpStatus } from "./enums";

export const codeMessage = {
  [EHttpStatus.OK]: "The server successfully returned the requested data.",
  [EHttpStatus.CREATED]: "Create or modify data successfully.",
  [EHttpStatus.ACCEPTED]:
    "A request has entered the background queue (asynchronous task).",
  [EHttpStatus.NO_CONTENT]: "Delete data successfully.",
  [EHttpStatus.BAD_REQUEST]:
    "There was as error in the request sent, and the server did not create or modify data.",
  [EHttpStatus.UNAUTHORIZED]:
    "The admin does not have permission please try to login again.",
  [EHttpStatus.FORBIDDEN]: "The admin is authorized, but access is forbidden.",
  [EHttpStatus.NOT_FOUND]:
    "The request sent is for a record the does not exist, and the server is not operating.",
  [EHttpStatus.NOT_ACCEPTABLE]: "The requested format is not available.",
  [EHttpStatus.GONE]:
    "The requested resource has been permanently deleted and will no longer be available.",
  [EHttpStatus.UNPROCESSABLE_ENTITY]:
    "When creating an object, a validation error occurred.",
  [EHttpStatus.INTERNAL_SERVER_ERROR]:
    "An error occurred in the server, please check the server.",
  [EHttpStatus.BAD_GATEWAY]: "Gateway error.",
  [EHttpStatus.SERVICE_UNAVAILABLE]:
    "The service is unavailable, the server is temporarily overloaded or maintained.",
  [EHttpStatus.GATEWAY_TIMEOUT]: "The gateway has timed out.",
};
