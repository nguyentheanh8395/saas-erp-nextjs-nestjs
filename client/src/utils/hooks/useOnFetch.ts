import { useState } from "react";

const useOnFetch = () => {
  const [isLoading, setIsLoading] = useState();
  return { isLoading };
};

export default useOnFetch;
