"use client";

import React, { useState } from "react";
import { SessionProvider } from "next-auth/react";

const NextAuthRegistry = ({ children }: React.PropsWithChildren) => {
  const [interval, setInterval] = useState(0);

  return (
    <SessionProvider refetchInterval={interval}>{children}</SessionProvider>
  );
};

export default NextAuthRegistry;
