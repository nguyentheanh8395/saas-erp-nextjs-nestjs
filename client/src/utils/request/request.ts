import axios from "axios";
import { IRequestCreate, IRequestList, IRequestRead } from "..";

axios.defaults.baseURL = process.env.baseApiURL;
axios.defaults.withCredentials = true;

const request = {
  create: async function <T>({ entity, payload }: IRequestCreate<T>) {
    console.log(
      "🚀 Create Request 🚀 ~ file: request.js ~ line 19 ~ create: ~ jsonData",
      payload
    );
    try {
      const reponse = await axios.post(`${entity}/create`, payload);
      return reponse.data;
    } catch (error) {
      return null;
    }
  },
  read: async function ({ entity, id }: IRequestRead) {
    try {
      const reponse = await axios.get(`${entity}/read/${id}`);
      return reponse.data;
    } catch (error) {
      return null;
    }
  },
  list: async function ({ entity, params = { page: 1 } }: IRequestList) {
    try {
      console.log("base url", process.env.baseApiURL);
      const response = await axios.get(`${entity}/list`, { params });
      return response;
    } catch (error) {
      console.log(error);
      return null;
    }
  },
};

export default request;
