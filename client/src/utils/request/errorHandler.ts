import { notification } from "antd";
import { AxiosError } from "axios";
import { IError, codeMessage } from "..";

const errorHandler = (error: AxiosError<IError>) => {
  const { response } = error;

  if (response?.status) {
    const message = response.data?.message;
    const { status } = response;
    const errorText = message || (codeMessage as any)[status];
    notification.config({
      duration: 10,
    });
    notification.error({
      message: `Request error ${status}`,
      description: errorText,
    });
    if (response.data && response.data.jwtExpired) {
      // history.push("/logout");
    }
    return response.data;
  } else {
    notification.config({ duration: 5 });
    notification.error({
      message: "No internet connection",
      description: "Cannot connect to the server, Check your internet network",
    });
    return {
      success: false,
      result: null,
      message: "Cannot connect to the server. Check your internet network",
    };
  }
};

export default errorHandler;
