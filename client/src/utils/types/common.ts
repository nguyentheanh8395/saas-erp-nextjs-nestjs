import { ReactNode } from "react";

export interface IRequest {
  entity: string;
}

export interface IRequestCreate<T> extends IRequest {
  payload: T;
}

export interface IRequestRead extends IRequest {
  id: string | number;
}

export interface IParamsList {
  page: number;
}

export interface IRequestList extends IRequest {
  params?: IParamsList;
}
export interface IError {
  message: string;
  jwtExpired: boolean;
}

export type TColumn = {
  title: ReactNode;
  dataIndex: string | Array<string>;
};

export type TConfigTable<T = void> = {
  entity: string;
  dataTableTitle: string;
  ADD_NEW_ENTITY?: string;
  dataTableColumns: Array<TColumn>;
  render?: (record: T, index: number) => void;
};
