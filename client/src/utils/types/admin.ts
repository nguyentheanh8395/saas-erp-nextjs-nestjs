export type TAdminLoginFormValue = {
  email: string;
  password: string;
  remember?: boolean;
};
