import {
  PropsWithChildren,
  createContext,
  useContext,
  useMemo,
  useReducer,
} from "react";
import { initialState, contextReducer } from "./reducer";

const CrudContext = createContext([]);

function CrudContextProvider({ children }: PropsWithChildren) {
  const [state, dispatch] = useReducer(contextReducer, initialState);
  const value = useMemo(
    () => [state, dispatch],
    [state, dispatch]
  ) as Array<never>;

  return <CrudContext.Provider value={value}>{children}</CrudContext.Provider>;
}

function useCrudContext() {
  const context = useContext(CrudContext);
  if (context === undefined) {
    throw new Error("useCrudContext must be used within a CrudContextProvider");
  }
  const [state, dispatch] = context;
}

export { CrudContextProvider, useCrudContext };
