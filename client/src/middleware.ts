import { getToken } from "next-auth/jwt";
import { NextResponse, type NextRequest } from "next/server";

export async function middleware(request: NextRequest) {
  const res = NextResponse.next();
  // const { pathname } = request.nextUrl;
  // const token = await getToken({ req: request });
  // const isLoginPath = pathname === "/login";
  // if (isLoginPath && token) {
  //   return NextResponse.redirect(new URL("/", request.url));
  // }
  // if (!isLoginPath && !token) {
  //   return NextResponse.redirect(new URL("/login", request.url));
  // }
  return res;
}

export const config = {
  matcher: ["/((?!api|_next/static|_next/image|favicon.ico|images).*)"],
};
