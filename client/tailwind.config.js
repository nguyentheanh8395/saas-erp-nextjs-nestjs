/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx,mdx}"],
  theme: {
    extend: {
      colors: {
        chambray: "#4F5D75",
        lynch: "#677788",
        "pale-blue": "#edf0f5",
        "gray-35": "#595959",
        "mainly-blue": "#22075e",
      },
      padding: {
        7.5: "1.875rem",
        18: "4.5rem",
        38: "9.5rem",
        50: "12.5rem",
      },
      margin: {
        7.5: "1.875rem",
        17: "4.25rem",
        17.5: "4.375rem",
        25: "6.25rem",
      },
      height: {
        26.5: "6.625rem",
      },
      minWidth: {
        50: "12.5rem",
      },
      maxWidth: {
        27.5: "27.5rem",
        100: "25rem",
        250: "62.5rem",
        275: "68.75rem",
      },
      boxShadow: {
        classic: "0px 0px 20px 3px rgba(150, 190, 238, 0.15)",
        active: "0px 0px 30px 8px rgba(150, 190, 238, 0.25)",
        "avatar-active": "0px 0px 10px 4px rgba(150, 190, 238, 0.3)",
      },
      zIndex: { 1000: 1000, 99: 99 },
    },
  },
  plugins: [],
};
